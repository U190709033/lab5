public class GCDRec {
    public static void main(String[] args) {
        int arg1 = Integer.parseInt(args[0]);
        int arg2 = Integer.parseInt(args[1]);
        int GCD;
        if (arg2 > arg1)
            GCD = GCDRec(arg2, arg1);
        else
            GCD = GCDRec(arg1, arg2);
        System.out.println(GCD);
    }
    public static int GCDRec(int number1 , int number2) {
        int r = number1 % number2;
        if (r == 0)
            return number2;
        else
            number2 = r;
            return GCDRec(number1, number2);
    }
}
