public class GCDLoop {
    public static void main(String[] args) {
        int number1 = Integer.parseInt(args[0]);
        int number2 = Integer.parseInt(args[1]);
        int r = -1;
        if (number1 > number2) {
            while (r != 0) {
                r = number1 % number2;
                if (r != 0)
                    number2 = r;
            } System.out.println(number2);
        }
        else {
            while (r != 0) {
                r = number2 % number1;
                if (r != 0)
                    number1 = r;
            } System.out.println(number1);
        }
    }
}
